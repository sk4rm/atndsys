import React, {Component} from 'react';
import {Button, Modal, Typography, message} from 'antd';
import './header.css';
import logo from '../../assets/i-Creatorz-Logo-White.png';
import {Redirect} from "react-router-dom";

const {Title} = Typography;

class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            headerTitleClass: "header-title",
            visible: false,
            redirect: false,
            redirect1: false,
            homeBtnVisibility: {display: "inline-block"}
        };
    }

    componentDidMount() {
        if (this.props.index === "dashboard") {
            this.setState({homeBtnVisibility: {display: "none"}})
        }
        setTimeout(() => {
            this.setState({headerTitleClass: "header-title slidein"});
        }, 1200);
    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    };

    backToDashboard = () => {
        this.setState({
            redirect1: true
        });
    };

    handleOk = e => {
        localStorage.removeItem('atndsysID');
        localStorage.removeItem('atndsysPW');
        message.info('Logging you out...');
        setTimeout(() => {
            this.setState({redirect: true});
        }, 2000);
    };

    handleCancel = e => {
        this.setState({
            visible: false,
        });
    };

    render() {
        let redirectElem;
        if (this.state.redirect) {
            redirectElem = <Redirect to={'/'}/>
        }

        let redirectElem1;
        if (this.state.redirect1) {
            redirectElem1 = <Redirect to={'/dashboard'}/>
        }

        return <div className="header-sec">
            {redirectElem}
            {redirectElem1}
            <Modal
                title="Logout from AtndSys"
                visible={this.state.visible}
                onOk={this.handleOk}
                onCancel={this.handleCancel}
                footer={[
                    <Button key="submit" type="primary" onClick={this.handleOk}>
                        Logout
                    </Button>
                ]}
            >
                Confirm logout from system?
            </Modal>
            <div className="header-sec1">
                <div className="header-logo">
                    <img src={logo} alt="Club Logo" draggable={false}/>
                </div>
                <div className="header-name">
                    <Title level={4} className="header-name-content">AtndSys</Title>
                </div>
                <div className="header-space">
                    <Title level={4} className="header-space-content">|</Title>
                </div>
                <div className={this.state.headerTitleClass}>
                    <Title level={4} className="header-title-content">{this.props.title}</Title>
                </div>
            </div>
            <div className="header-sec2">
                <div className="header-ops-sec">
                    <div className="header-login-sec">
                        <Button type="primary" shape="circle" icon="home" onClick={() => {this.backToDashboard()}} style={this.state.homeBtnVisibility}/>
                        <Button type="primary" shape="round" icon="logout" onClick={() => {this.showModal()}}>
                            Logout
                        </Button>
                    </div>
                </div>
            </div>
        </div>
    }
}

export default Header;
