import React, {Component} from 'react';
import {Spin, Icon, Modal, Button, Form, Input} from 'antd';
import 'antd/dist/antd.css';
import './login.css';
import logo from '../../assets/i-Creatorz-Logo-White.png';
import {Redirect} from "react-router-dom";

const antIcon = <Icon type="loading" style={{fontSize: 24, color: "blueviolet"}} spin/>;

function handleChange(e) {
    e.form.validateFields((err, values) => {
        if (!err) {
            localStorage.setItem('atndsysID',values.userid);
            localStorage.setItem('atndsysPW',values.password);
        }
    });
}

class WrappedFormLogin extends React.Component {
    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Form className="login-form">
                <Form.Item>
                    {getFieldDecorator('userid')(
                        <Input
                            prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            placeholder="User ID"
                            onChange={handleChange(this.props)}
                        />,
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('password')(
                        <Input
                            prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            placeholder="Password"
                            type="password"
                            onChange={handleChange(this.props)}
                        />,
                    )}
                </Form.Item>
            </Form>
        );
    }
}

const LoginForm = Form.create()(WrappedFormLogin);

class Login extends Component {
    state = { visible: false };

    showModal = () => {
        this.setState({
            visible: true,
        });
    };

    handleOk = e => {
        this.setState({
            visible: false,
        });
        this.doAuth();
    };

    constructor(props) {
        super(props);
        this.state = {
            redirect: false,
            loginClass: "login-center"
        };
    }

    componentDidMount() {
        document.title = "AtndSys Login";
        if (localStorage.getItem('atndsysID') == null || localStorage.getItem('atndsysPW') == null || localStorage.getItem('atndsysID') === "undefined" || localStorage.getItem('atndsysPW') === "undefined") {
            this.showModal();
        } else {
            this.doAuth();
        }
    }

    doAuth() {
        fetch('https://atndsys-server.now.sh/authtest?id=' + localStorage.getItem('atndsysID') + '&pw=' + localStorage.getItem('atndsysPW')).then(response => response.text()).then(data => {
            if (data !== 'true') {
                this.showModal();
            } else {
                this.setState({loginClass: "login-center fadeout"});
                setTimeout(() => {
                    this.setState({redirect: true});
                }, 500);
            }
        });
    }

    render() {
        let redirectElem;
        if (this.state.redirect) {
            redirectElem = <Redirect to={'/dashboard'}/>
        }
        return (
            <div className="login">
                <Modal
                    title="Login to AtndSys"
                    visible={this.state.visible}
                    onOk={this.handleOk}
                    footer={[
                        <Button key="submit" type="primary" onClick={this.handleOk}>
                            Login
                        </Button>
                    ]}
                >
                <LoginForm />
                </Modal>
                {redirectElem}
                <div className={this.state.loginClass}>
                    <div className="login-logo">
                        <img src={logo} alt="Club Logo" draggable={false}/>
                    </div>
                    <div className="login-spin">
                        <Spin indicator={antIcon}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;
